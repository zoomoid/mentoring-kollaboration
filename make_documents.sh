#!/bin/bash/
mkdir -p ./build/documents

cd ./src/documents/
for f in *.tex; do
  latexmk --interaction=nonstopmode -pdf $f
  mv ${f%.tex}.pdf ../../build/documents
done;

rm *.log -f
rm *.aux -f
rm *.fls -f
rm *.fdb_latexmk -f
rm *.out -f
rm *.snm -f
rm *.nav -f
rm *.toc -f
rm *.pdf -f