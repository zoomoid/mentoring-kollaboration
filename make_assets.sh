#!/bin/bash
# files=("erinnerungskurve" "vergessenskurve" "lernkurve")

build(){
  cd ./src/assets/
  for f in "*.tex"; do
    input="${f%.*}.tex"
    output="${f%.*}.pdf"
    latexmk -pdf $input
    mv $output ../../build/assets
  done;
  cd ../../
}

clean(){
  cd ./src/assets/
  rm *.log -f
  rm *.aux -f
  rm *.fls -f
  rm *.fdb_latexmk -f
  rm *.out -f
  rm *.snm -f
  rm *.nav -f
  rm *.toc -f
  rm *.pdf -f
  cd ../../
}

_convert(){
  cd ./build/assets/
  for file in *.pdf; do
    convert -density 300 -quality 100 -flatten $file ${file%.pdf}.png
  done;
  # cd ../
}


mkdir -p ./build/assets
if [ "$1" = "build" ] 
then
  build
  clean
  if [ "$2" = "convert" ] 
  then
    _convert
  fi
elif [ "$1" = "convert" ] 
then
  _convert
fi
echo "Exiting."