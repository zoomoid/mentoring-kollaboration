\documentclass[10pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage[a4paper,left=2cm,right=2cm,top=2cm,bottom=2cm]{geometry}
\usepackage{amath}
\usepackage{bmath}
\usepackage{enumitem}
\usepackage{float}
\pagenumbering{gobble}
\title{}
\author{}
\date{}
\setlength{\parindent}{0pt}
\setlength{\parskip}{0.8em}
\begin{document}
    \begin{center}
        {\footnotesize (Aus Thomas, Diskrete Strukturen, 2018, 67ff.)}
    \end{center}
    \section*{Halbgruppen und Monoide}
    Als nächstes wollen wir uns davon lösen, Verknüpfungen als eigenständige Objekte zu betrachten. Wir wollen
    den Standpunkt einnehmen, dass Verknüpfungen „fest“ zu einer Menge dazugehören, und wollen die Menge
    zusammen mit den Verknüpfungen als eine gemeinsame „algebraische Struktur“ ansehen.
    Obwohl wir auf \(\N\) und \(\N_0\) mehrere uns vertraute Verknüpfungen haben, begnügen wir uns zunächst mit 
    „einfacheren“ Strukturen und studieren Mengen, die mit genau einer Verknüpfung versehen sind und einige der
    gerade eingeführten Eigenschaften erfüllen. Mengen, welche mit zwei miteinander verträglichen Verknüpfungen
    ausgestattet sind, werden dann später eingeführt.

    \setlength{\parskip}{0.8em}
    \textbf{(6.17) Definition} (Halbgruppe, kommutative Halbgruppe, Monoid)\textbf{.}
    \setlength{\parskip}{0pt}
    \begin{enumerate}[label=(\alph*)]
        \item Eine \emph{Halbgruppe} besteht aus einer Menge \(M\) zusammen mit einer assoziativen Verknüpfung \(m\) auf \(M\). Unter Missbrauch der Notation bezeichnen wir sowohl die besagte Halbgruppe als auch die unterliegende Menge mit \(M\). Die Verknüpfung \(m\) wird \emph{Multiplikation} (oder \emph{Halbgruppenverknüpfung}) von \(M\) genannt.
        \item Eine Halbgruppe \(M\) heißt \emph{kommutativ}, falls die Multiplikation von \(M\) kommutativ ist.
        \item Ein \emph{Monoid} ist eine Halbgruppe \(M\), welche ein neutrales Element bzgl. \(\cdot^M\) besitzt. Die Halbgruppenverknüpfung eines Monoids \(M\) wird auch \emph{Monoidverknüpfung} von \(M\) genannt. Das neutrale Element bzgl. der Multiplikation wird auch \emph{Eins} (oder \emph{Einselement}) von \(M\) genannt und als \(1  = 1^M\) notiert.
    \end{enumerate}
    Bei der Festlegung „\(\cdot = \cdot^M := m\)“ in Definition (6.17)(a) für die Multiplikation einer Halbgruppe handelt es sich um eine Notation, um in einer abstrakt (d.h. nicht in einem konkreten Beispiel) gegebenen Halbgruppe einfach von der Verknüpfung sprechen zu können und diese nicht immer explizit erwähnen zu müssen. In der Regel werden wir also von einer „Halbgruppe \(M\)“ anstatt von einer „Halbgruppe \(M\) mit Multiplikation \(m\)“ sprechen, die Multiplikation als implizit gegeben ansehen und diese dann mit dem Symbol \(\cdot\) bezeichnen. Die Bezeichung \(\cdot^M\) werden wir nur dann verwenden, wenn wir explizit darauf hinweisen möchten, dass diese Multiplikation zu \(M\) gehört (etwa, wenn wir mehrere Halbgruppen auf einmal betrachten); in der Regel werden wir jedoch darauf verzichten.

    Die Notationen „·“ und „1“ sowie auch die Bezeichnungen „Multiplikation“ und „Eins“ sind von Beispielen wie
    dem der natürlichen Zahlen motiviert. Es gibt auch andere Beispiele, wo die Halbgruppenverknüpfung keine
    Multiplikation im vertrauten Sinne ist. In diesen konkret gegebenen Beispielen verwenden wir weiterhin die
    jeweils vorliegende Notation, die durch das Beispiel mitgebracht wird; siehe insbesondere Bemerkung (6.22).
    Mit Hilfe der Standardnotation in einer Halbgruppe  \(M\) liest sich die Assoziativität der Multiplikation 
    wie folgt:

    \begin{itemize}
        \item \emph{Assoziativität.} Für \(x,y,z \in M\) ist \(x(yz) = (xy)z\).
    \end{itemize}
    Ist eine Halbgruppe \(M\) kommutativ, so gilt neben der Assoziativität zusätzlich noch:
    \begin{itemize}
        \item \emph{Kommutativität.} Für \(x,y \in M\) ist \(xy = yx\).
    \end{itemize}
    Mit Hilfe der Standardnotation in einem Monoid \(M\) lesen sich die \emph{Axiome}, d.h. dessen definierende Eigenschaften, wie folgt:
    \begin{itemize}
        \item \emph{Assoziativität.} Für \(x,y,z \in M\) ist \(x(yz) = (xy)z\).
        \item \emph{Existenz der Eins.} Es existiert ein \(e \in M\), dass für \(x \in M\) stets \(ex = xe = x\) gilt. Dieses \(e\) ist nach Bemerkung (6.11) eindeutig bestimmt und wird mit 1 bezeichnet. Wir haben also \(1x = x1 = x\) für \(x \in M\).
    \end{itemize}
    Da Monoide insbesondere Halbgruppen sind, erhalten wir auch den Begriff eines \emph{kommutativen Monoids}.
    Neben der Multiplikation auf den natürlichen Zahlen ist auch die Addition assoziativ und kommutativ. Für kommutative
    Halbgruppen, Monoide und Gruppen haben sich daher noch andere Bezeichnungen und Schreibweisen
    eingebürgert:

    \setlength{\parskip}{0.8em}
    \textbf{(6.18) Definition} (abelsche Halbgruppe, abelsches Monoid)\textbf{.}
    \setlength{\parskip}{0pt}
    \begin{enumerate}[label=(\alph*)]
        \item Eine \emph{abelsches Halbgruppe} ist eine kommutative Halbgruppe \(A\) mit Halbgruppenverknüpfung \(+ = +^A\), genannt \emph{Addition} von \(A\)
        \item Ein \emph{abelsches Monoid} ist eine abelsche Halbgruppe \(A\), welche ein neutrales Element bzgl. \(+^A\) besitzt. Das neutrale Element bzgl. der Addition wird auch \emph{Null} (oder \emph{Nullelement}) von \(A\) genannt und als \(0 = 0^A\) notiert
    \end{enumerate}
    Eine abelsche Halbgruppe ist also strukturell gesehen das Gleiche wie eine kommutative Halbgruppe; wir verwenden
    lediglich in abstrakten abelschen Halbgruppen eine andere Standardnotation: Abstrakte Halbgruppen
    (die ggf. auch mal kommutativ sein dürfen, aber im Allgemeinen nicht müssen) werden multiplikativ geschrieben,
    abstrakte abelsche Halbgruppen werden additiv geschrieben.

    Insbesondere gilt: Alle Aussagen über beliebige Halbgruppen und über kommutative Halbgruppen (in multiplikativer
    Notation geschrieben) bleiben auch für abelsche Halbgruppen (in additiver Notation geschrieben)
    korrekt. Umgekehrt bleiben alle Aussagen über abelsche Halbgruppen (in additiver Notation geschrieben) auch
    für kommutative Halbgruppen (in multiplikativer Notation geschrieben) korrekt. Bei der Verwendung solcher
    Aussagen muss gegebenenfalls nur die jeweilige Notation angepasst werden. In der Regel werden wir getroffene
    Aussagen über Halbgruppen nicht für abelsche Halbgruppen in additiver Notation wiederholen.
    
    Mit Hilfe der Standardnotation in einer abelschen Halbgruppe \(A\) lesen sich deren Axiome wie folgt:
    \begin{itemize}
        \item \emph{Assoziativität.} Für \(x,y,z \in A\) ist \(x + (y + z) = (x + y) + z\).
        \item \emph{Kommutativität.} Für \(x,y \in A\) ist \(x + y = y + x\)
    \end{itemize}
    Die Axiome eines abelschen Monoids \(A\) sind die eines kommutativen Monoids in additiver Notation:
    \begin{itemize}
        \item \emph{Assoziativität.} Für \(x,y,z \in A\) ist \(x + (y + z) = (x + y) + z\).
        \item \emph{Existenz einer Null.} Es existiert ein \(n \in A\) derart, dass für \(x \in A\) stets \(n + x = x + n = x\) gilt. Dieses \(n\) ist nach Bemerkung (6.11) eindeutig bestimmt und mit 0 bezeichnet. Wir haben also \(0 + x = x + 0 = x\) für \(x \in A\).
        \item \emph{Kommutativität.} Für \(x,y \in A\) ist \(x + y = y + x\)
    \end{itemize}
    Vom Rechnen in den natürlichen Zahlen sind wir es gewohnt, bei Produkten aus mehreren Faktoren bzw.
    Summen aus mehreren Summanden keine Klammern zu setzen. Dies ist durch die Assoziativität gerechtfertigt,
    da verschiedene Klammerungen zum selben Wert führen würden. Wir übertragen diese Konvention auf den
    allgemeinen Fall:

    \setlength{\parskip}{0.8em}
    \textbf{(6.19) Konvention.} Wegen der Assoziativität der Multiplikation einer Halbgruppe bzw. der Addition einer abelschen Halbgruppe kommt es bei iterierter Bildung nicht auf die Klammerung an. Im Regelfall lassen wir daher die Klammern im Folgenden weg.

    Nach wir alle in Satz (6.1)(a), (b) auftauchenden Phänomene analysiert und von den konkreten Beispielen \(\N\) und \(\N_0\) abstrahiert haben, lassen sich diese nun kurz wie folgt reformulieren.
    
    \textbf{(6.20) Beispiel.}
    \setlength{\parskip}{0pt}
    \begin{enumerate}[label=(\alph*)]
        \item \begin{enumerate}[label=(\roman*)]
            \item Die Menge \(\N\) zusammen mit der üblichen Addition ist eine abelsche Halbgruppe, aber kein abelsches Monoid
            \item Die Menge \(\N\) zusammen mit der üblichen Multiplikation ist ein kommutatives Monoid. Die Eins von \(\N\) ist die übliche Eins.
        \end{enumerate}
        \item \begin{enumerate}[label=(\roman*)]
            \item Die Menge \(\N_0\) zusammen mit der üblichen Addition ist ein abelsches Monoid. Die Null von \(\N_0\) ist die übliche Null.
            \item Die Menge \(\N_0\) zusammen mit der üblichen Multiplikation ist ein kommutatives Monoid. Die Eins von \(\N_0\) ist die übliche Eins.
        \end{enumerate}
    \end{enumerate}
    \textbf{(6.21) Beispiel.} Es gibt ein nicht-kommutatives Monoid mit genau drei Elementen, dessen Multiplikation durch folgende Verknüpfungstafel gegeben ist.

    \begin{table}[H]
        \begin{tabular}{c|ccc}
            \(\cdot\) & 1 & \(c_1\) & \(c_2\) \\ \hline
            1 & 1 & \(c_1 \) & \(c_2\) \\
            \(c_1\) & \(c_1\) & \(c_1\) & \(c_1\) \\
            \(c_2\) & \(c_2\) & \(c_2\) & \(c_2\) \\        
        \end{tabular}
    \end{table}
\end{document}