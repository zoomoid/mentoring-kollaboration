\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[top=2.5cm,left=2.5cm,bottom=2.5cm,right=2.5cm,a4paper]{geometry}
\usepackage[ngerman]{babel}
\usepackage[linktocpage=true]{hyperref}
\usepackage{enumitem,graphicx,float,helvet}
\renewcommand{\familydefault}{\sfdefault}
\setlength\parindent{0pt}
\setlength\parskip{0.6em}
\title{RWTHonline-Tutorial}
\author{Alexander Bartolomey}
\date{\today}
\begin{document}
  \maketitle

  \section*{Einleitung}
  Mit dem WS18/19 hat die RWTH Aachen entschieden, das bisherige Verwaltungs-Tool für so ziemlich jede studentische Aktivität an der Uni durch ein neues Tool, \textsf{RWTHonline} zu ersetzen. Das dies notwendig war, ist zumindest teilweise nachvollziehbar, das alte CampusOffice-System ist immerhin seit 2001 im Einsatz gewesen und entsprach zu keinem Zeitpunkt den Anforderungen, die insgesamt daran gestellt wurden. 
  
  Allerdings ist bei diesem Wechsel zu \textsf{RWTHonline}, der nunmehr fließend seit eineinhalb Jahren vollzogen wird, mit einem einjährigen Pilotbetrieb für ausgewählte Studiengänge, aus dem aber gefühlt keine Änderungen oder Verbesserungen umgesetzt wurden, so ziemlich alles schief gegangen, was bei so einer Software-Umstellung schief gehen kann. 

  Das Produkt \textsf{CampusOnline}, dass von der TU Graz entwickelt wird und von der RWTH lizenziert wird mit, mit der Erlaubnis, den Namen entsprechend zu personalisieren, hat die RWTH bis jetzt circa 6,1 Millionen Euro gekostet und wird aufgrund der laufenden Lizenzgebühren noch bis 2023 mit jährlich 350.000 Euro am Leben unterstützt.

  Nachfolgend werde ich versuchen, möglichst kleinschrittig zu erklären, wie RWTHonline zu benutzen ist, sollten zu irgendeinem Zeitpunkt Fragen aufkommen, so können diese über einen beliebigen Kanal gestellt werden.

  \section*{Einführung in \textsf{RWTHonline}}
  Zuerst ein paar notwendige, da verkürzende, Vokabeln:
  \subsection*{Präambel}
  Wenn von \emph{LV-Anmeldung} die Rede ist, so meint dies \emph{Lehrveranstaltungs-Anmeldung}, dieses LV wird an vielen Stellen verwendet, ohne das es wirklich jemals erklärt wird. Relativ uninteressant verbirgt sich dahinter einfach eine beliebige Veranstaltung, die Studenten wahrnehmen können.

  Hinter der ebenfalls auftretenden Abkürzung \emph{SPO} steckt das Wort \emph{Studienprüfungsordnung}. Ohne zu weit ins Detail zu gehen: Zu jedem Studiengang gibt es eine Prüfungsordnung, unter der ihr euch einschreibt und die ab dann für euch gilt, die vorschreibt, was, wie, wo, wann etc. geprüft wird. Insbesondere stehen in dieser festgeschrieben, welche Module von Studierenden zu absolvieren sind, ebenso wie die Rechte, die ihr als Studierende des Fachs Informatik habt. Weil bei Kombi-Vorlesungen, die mehrere Studiengänge gleichzeitig hören können, die Platzvergabe, sofern diese limitiert ist, irgendwie fair geregelt werden muss, muss man sich bei der Anmeldung für ein Modul mit seiner jeweiligen Prüfungsordnung anmelden, damit die Platzvergabe vernünftig aufgeteilt werden kann.

  \subsection*{Modulkatalog des 1. Semesters}
  Die ersten Semester in der Informatik haben den Vorteil, dass (relativ) vorgeschrieben ist, was ihr hört und in welcher Reihenfolge. Diese wurde so gewählt, um möglichst aufeinander aufbauend Wissen zu vermitteln.
  
  Für das erste Semester ist vorgesehen, dass ihr die Veranstaltungen
  \begin{itemize}
    \item \emph{Analysis für Informatiker}, kurz AfI, und
    \item \emph{Diskrete Strukturen}, kurz DS, aus dem Modulbereich \emph{Mathematik},
    \item \emph{Einführung in die Technische Informatik}, kurz TI, aus dem Bereich \emph{Technische Informatik} und
    \item \emph{Programmierung}, kurz Progra, aus dem Modulbereich \emph{Praktische Informatik}
  \end{itemize} hört, sowie das für alle Erstsemester der Informatik verpflichtende \emph{\textbf{Mentoring}}, zu finden im Abschnitt \emph{Sonstige Leistung}.
  \subsection*{Anmeldung bei RWTHonline}
  Wie an jedem IT-Service der RWTH meldet man sich auch bei RWTHonline\footnote{ https://online.rwth-aachen.de/} mit der TIM-Kennung und dem im IT-SelfService\footnote{ https://www.rwth-aachen.de/selfservice/} gesetzten Passwort an. Ist dies noch nicht passiert, so pausiere bitte jetzt und setze dein Passwort für die IT-Services der RWTH, insbesondere für CAMPUS-Office (noch falsch benannt), die Webdienste und die E-Learning-Plattform \(L^2P\).
  \begin{center}
    \emph{Bitte denke daran, starke Passwörter zu verwenden, da der Zugriff auch von außerhalb der RWTH-Netzwerke erfolgen kann und damit ein schwaches Passwort eine Angriffstelle für Account-Diebstahl oder schlimmeres bieten kann! Tipps für Passwörter gibt es im ersten Mentoring-Termin.}
  \end{center}
  Beim ersten Anmelden wird die Website euch (vielleicht) anzeigen, welche Daten zwischen den RWTH-Services über euch ausgetauscht werden. Dies ist prinzipiell uninteressant, solange ihr euch an einem Service der RWTH selbst anmeldet. Mit weiteren Button kann fortgefahren werden. Damit solltet ihr nach einigen weiteren Augenblicken auf einer Startseite aus Kacheln landen, die euch verschiedene Optionen anbieten.

  \begin{figure}[H]
    \includegraphics[width=\textwidth]{./img/rwthonline-1.png}
    \caption{Startseite von RWTHonline}
    \label{fig:startseite}
  \end{figure}

  Ihr habt hier Zugriff auf die meisten administrativen Tools wie Terminkalender, Studienbescheinigungen, Rückmeldung und Studienbeitragsstatus, Ändern von Meldeadressen und in Zukunft auch auf Notenspiegel, Studienleistungen und Prüfungsanmeldungen.

  \subsection*{Studienübersicht (Curriculum Support)}
  Das momentan hilfreichste Mikro-Tool von RWTHonline ist die Studienübersicht, da diese gefiltert für euch persönlich anzeigt, welche Fächer ihr belegen könnt. Mit einem Klick auf die Kachel navigieren wir zu der dafür geschaffenen Website.

  \begin{figure}[H]
    \includegraphics[width=\textwidth]{./img/rwthonline-2.png}
    \caption{Curriculum Support}
    \label{fig:curriculumsupport}
  \end{figure}

  Diese baumartige Struktur erlaubt es nun, gezielt zu den Fächern zu navigieren, die zuvor aufgelistet wurden. Ein Klick auf das Plus oder den jeweiligen Knotennamen öffnet das Unterverzeichnis.

  Unter dem zweiten Dropdown-Menü über dieser Tabelle (in Abb. \ref{fig:curriculumsupport} mit Inhalt \emph{Knoten (Alle)}), kann auch noch ein Modus \emph{Semesterplan} ausgewählt werden, der alle Module so gruppiert, wie sie theoretisch im Studienverlaufsplan\footnote{ https://www.informatik.rwth-aachen.de/global/show\_document.asp?id=aaaaaaaaaatsucn} aufgelistet sind, allerdings ist die Zuteilung der Module auf die Semester momentan noch nicht vollständig und/oder richtig. Daher wählen wir uns die Module manuell aus.

  \subsection*{Planung von Terminen und Stundenplan}
  RWTHonline erlaubt es, sich einen Stundenplan im Voraus zu planen, ohne dafür Papier benutzen zu müssen. Dafür kann man Veranstaltungen, die man evtl. besuchen möchte, in seinem Kalender vormerken. Dafür ist auf der Detail-Seite, was das ist, wird weiter unten erklärt, neben dem Namen der LV immer ein Stern zu sehen, mit einem Klick auf diesen wird die Veranstaltung \emph{vorgemerkt} und ist unter der Kachel \emph{Lehrveranstaltungen} unter dem Tab \emph{Meine vorgemerkten Lehrveranstaltungen} gelistet. Diese Funktion kann man benutzen, um modulare LVs wie Tutorien zu planen oder Module, für die noch keine Anmeldung offen ist, für später vorzumerken, um das erneute Navigieren zu sparen oder die Termine bereits schonmal in den Kalender aufzunehmen, aber auch dazu später mehr. 

  Wichtig ist nur, zu merken, dass diese Vormerk-Funktion bei jeder LV existiert und dafür genutzt werden kann, Veranstaltungen für sich zu markieren.

  \subsection*{Anmeldung am Beispiel \emph{Analysis für Informatiker}}
  \subsubsection*{Übersicht über die LVs}
  AfI bietet sich für eine exemplarische Anmeldung an, da es als bisher einziges Fach alle möglichen Anmeldeverfahren anbietet.

  Für AfI sind sowohl Vorlesung als auch Globalübung als kombinierte Anmeldung verfügbar, ebenso wie Kleingruppenübungen, auch Tutorien genannt.

  Klappen wir also zuerst die Verzeichnisse soweit auf, dass wir für AfI Prüfung und Kleingruppenübungen (grüne Knoten) sowie Vorlesung und Globalübung (rote Dreiecksknoten) sehen.

  Die Farben repräsentieren dabei (eigentlich) verschiedene Veranstaltungstypen, grün sollen Prüfungen sein, rot sollen LV-Angebote sein, allerdings ist dieses Typisierungssystem momentan ebenfalls mehr als inkonsistent.
  \begin{figure}[H]
    \includegraphics[width=\textwidth]{./img/rwthonline-3.png}
    \caption{Angebotene Veranstaltungen für Analysis für Informatiker}
    \label{fig:afi-lv}
  \end{figure}

  Dass ein Anmeldeverfahren offen ist, erkennt man an dem grünen T rechts, dies ist ein Indikator für ein offenes Anmeldeverfahren. Mit einem Ampelmodell ist dann entsprechend ein gelbes T Indikator für in Anmeldeverfahren in der Zukunft. Ein graues T heißt momentan, dass für diese Veranstaltungen keine Informationen über ein zukünftiges Anmeldeverfahren vorhanden sind. Dies kann sich aber jederzeit ändern, da die Daten ohnehin nur graduell und schleichend aktualisiert werden, es empfiehlt sich also, häufiger mal nachzuschauen, wie der aktuelle Status ist, solltet ihr euch noch nicht für alle Veranstaltungen anmelden können.
  \subsubsection*{Anmeldung für Vorlesung und Übung}
  Mit einem Klick auf die jeweilige Veranstaltung geht dann noch einmal ein weiteres Unterverzeichnis auf in dem wir nun noch mehr Informationen in kompakter Form zu der gewählten Veranstaltung finden, unter anderem die Raumnummer, den Dozenten sowie das Datum und die Uhrzeit der ersten Veranstaltung (außerdem sind im Namen der Veranstaltung noch einige Informationen kodiert, diese sind allerdings vorerst uninteressant für uns).
  \begin{figure}[H]
    \includegraphics[width=\textwidth]{./img/rwthonline-4.png}
    \caption{Vorlesung für Analysis für Informatiker}
    \label{fig:afi-vl}
  \end{figure}

  Ist für diese Veranstaltung ein Anmeldeverfahren offen, so kommen wir mit einem Klick auf das grüne T direkt zu der Seite für die Anmeldung, diese wird sich in einem neuen Browser-Tab öffnen, sodass wir nicht wieder mühselig zurücknavigieren müssen. Klicken wir hingegen auf den Namen der Veranstaltung, landen wir auf einer Seite, die noch mehr Details zu dem Modul liefert (s. Abbildung \ref{fig:afi-vl-details}).
  \begin{figure}[]
    \includegraphics[width=\textwidth]{./img/rwthonline-5.png}
    \caption{Details zur Vorlesung für Analysis für Informatiker}
    \label{fig:afi-vl-details}
  \end{figure}

  Unter anderem findet sich hier meist eine Übersicht der Themen der Veranstaltung, eine Liste der Termine der Veranstaltung sowie in der SPO verankerte Details und Attribute der Vorlesung, die wir uns im Rahmen des Mentorings auch noch einmal genauer angucken werden. Außerdem können wir auch von hier aus zur Anmeldung für das Modul gelangen, wenn ein solches Anmeldeverfahren existiert, zu sehen an dem Indikator und dem anklickbaren Button links unter der Gliederung.

  Auf der ersten Seite der Anmeldung (zu sehen in Abbildung \ref{fig:afi-lv-anmeldung-details})sehen wir dann Details zum Anmeldeverfahren: Neben den Fristen sehen wir hier auch, wie die Plätze für die LV vergeben werden und wie viele Plätze existieren. Das Losverfahren ist dabei das Standardverfahren und wenn die Anzahl der Teilnehmer unbegrenzt oder sehr groß ist, muss man sich diesbezüglich auch keine Gedanken machen, keinen Platz zu kriegen. Da es sich um eine Vorlesung/Globalübung handelt, haben wir auch nicht die Möglichkeit, noch andere Gruppen auszuwählen und haben deshalb nur die Auswahl der Standardgruppe, zu der wir automatisch zugeteilt werden. 

  \begin{figure}[]
    \includegraphics[width=\textwidth]{./img/rwthonline-6.png}
    \caption{Details zur Anmeldung zu AfI}
    \label{fig:afi-lv-anmeldung-details}
  \end{figure}

  Mit einem Klick auf \emph{Weiter} gelangen wir zur nächsten Seite (zu sehen in Abbildung \ref{fig:afi-lv-anmeldung-auswahl}), auf der wir nun erkennen, dass wir uns gerade für Vorlesung \emph{und} Globalübung anmelden, wenn wir bis dahin nicht auf den (kleinen) Titel oben links geachtet haben, denn wir müssen, damit die Standardgruppe gewählt werden kann, einen Kontext angeben, aus dem wir uns anmelden, was eben gerade unsere Prüfungsordnung ist, die SPO. 
  \begin{figure}[]
    \includegraphics[width=\textwidth]{./img/rwthonline-7.png}
    \caption{Auswahl zur Anmeldung zu AfI}
    \label{fig:afi-lv-anmeldung-auswahl}
  \end{figure}

  Für die Vorlesung, den ersten Punkt, wählen wir aus, dass wir uns für die \emph{Vorlesung Analysis für Informatiker}, den nicht-ausgegrauten Punkt, anmelden, für die Übung, den zweiten Punkt, genau das gleiche, nur eben mit analogem Namen, \emph{Globalübung Analysis für Informatiker}.

  Sobald die Eingaben akzeptiert wurden, und nichts mehr fehlt, wird der \emph{Belegwunsch erfassen}-Knopf unten rechts auf der Seite anklickbar und die Hinweise darunter sollten verschwunden sein. In dem sich nun öffnenden Popup erhalten wir nochmals einen Überblick über die von uns getroffene Auswahl. Wir bestätigen diese erneut und haben uns damit für Vorlesung und Globalübung in AfI angemeldet. 

  Binnen der nächsten paar Minuten erhalten wir einige E-Mails, die uns dies bestätigen, diese können (meist) ohne weitere Bedenken gelöscht werden. Wenig später sollten dann auch die Termine der Veranstaltung, zu der ihr euch angemeldet habt, in eurem Kalender im RWTHonline auftauchen.

  Das Anmeldeverfahren für die Vorlesung und Globalübung ist damit abgeschlossen, bleiben bei AfI noch die Kleingruppenübungen.
  \subsubsection*{Anmeldung für die Kleingruppenübungen}
  Kleingruppenübungen sind etwas Besonderes, unter anderem dadurch, dass diese limitierte Gruppengrößen haben, wie der Name eigentlich bereits impliziert. Außerdem bieten diese Gruppen Individualisierungsmöglichkeiten für euren Stundenplan, d.h. es gibt meist viele verschiedene Termine, an denen KGÜs mit verschiedenen HiWis\footnote{ HiWi, kurz für Hilfswissenschaftler, sind meist Studentische Hilfskräfte, die den Lehrstühlen in der Lehre unter die Arme greifen} gehalten werden. Was explizit in diesen KGÜs gemacht wird, entscheidet der Lehrstuhl und wird meist in den ersten Vorlesungswochen vorgestellt. 

  Wenn ihr noch andere Termine, innerhalb oder außerhalb der Uni, habt, so bietet es sich an, die Tutorien so zu wählen, dass keine Kollisionen auftreten. Für die ersten Semester sind diese insbesondere so geplant, dass \emph{möglichst wenige Kollisionen} auftreten.\footnote{ Das Optimierungsproblem, das hier zugrunde liegt, lässt sich (bis jetzt) nicht effizient lösen und deshalb approximiert man es nur, d.h. die Ideallösung kann nicht genau getroffen werden}

  Wie zuvor auch wählen wir den Knoten \emph{Übung Analysis für Informatiker} aus
  \begin{figure}[H]
    \includegraphics[width=\textwidth]{./img/rwthonline-8.png}
    \caption{Kleingruppenübung für Analysis für Informatiker}
    \label{fig:afi-tut}
  \end{figure}

  Auch hier finden wir wieder das grüne T, was uns bestätigt, dass wir uns für die Kleingruppenübungen anmelden können. Auf der Detailseite der Veranstaltung sehen wir jetzt die verschiedenen Gruppen, die es gibt, zusammen mit den Uhrzeiten und Räumen, in denen die Tutorien stattfinden. Anhand dieser Liste könntet ihr nun auswählen, was euch am Besten passt in Kombination mit allen anderen Veranstaltungen. An dieser Stelle bietet es sich an, die Planung mit allen festen Veranstaltungen abzuschließen und dann die modularen Veranstaltungen, gerade die Tutorien in die Lücken zu füllen. Exemplarisch werden wir die Anmeldung für einige Gruppen vornehmen, die uns gut passen, insbesondere haben wir eine Gewichtung getroffen, welche Gruppe wir am liebsten hätten, da wir eben eine solche Priorisierung angeben werden müssen:
  \begin{itemize}
    \item Gruppe 11, donnerstags um 16:30 Uhr bis 18:00 Uhr, idealste Gruppe,
    \item Gruppe 14, mittwochs um 12:30 Uhr bis 14:00 Uhr, akzeptable Gruppe,
    \item Gruppe 2, dienstags um 16:30 Uhr bis 18:00 Uhr, akzeptable Gruppe,
    \item Gruppe 3, donnerstags um 10:30 Uhr bis 12:00 Uhr, im Zweifel auch noch okay.
  \end{itemize}
  Insbesondere haben wir auch exemplarisch einige Gruppen, an denen wir gar nicht können, zum Beispiel Gruppe 10, donnerstags um 8:30 Uhr.\footnote{Ganz ehrlich, wer sich um diese Uhrzeit schon AfI geben kann, hat meinen vollsten Respekt}
  
  Wie wir gleich sehen werden, ist es relevant, dass wir solche Priorisierungen treffen. Insbesondere sollte euch bewusst sein, dass \emph{Gar-Nicht-Können} dazu führen kann, dass bei der Gruppenvergabe, wenn keiner eurer Wunschslots mehr frei ist, und ihr alles andere als unmöglich gekennzeichnet habt, ihr keinen Platz in \emph{irgendeiner Gruppe erhaltet} und ggf. deshalb \emph{nicht die Prüfungsleistung erbringen könnt!} Diese Möglichkeit sollte daher mit Vorsicht genutzt werden.

  Wenn wir nun wieder zu der LV-Anmeldungsseite für die Kleingruppenübungen navigieren, wie wir es für die Vorlesung und Globalübung getan haben, entweder mit dem grünen T im Curriculum Support oder über die Modulseite selbst mit dem Button, haben wir auf Seite 1 wieder die Übersicht über alle möglichen Gruppen, für die wir uns anmelden können. Auch hier sind wieder alle wichtigen Informationen zu jeder einzelnen Gruppe gelistet. Auch sehen wir hier die maximale Anzahl von Teilnehmern pro Gruppe, sofern diese existiert. Für AfI sind das je 70 Teilnehmer pro Tutorium, bei 14 Tutorien macht das 980 Plätze, es sollte also jeder einen Platz bekommen. Mit einem Klick auf weiter kommen wir nun zu der Auswahlseite, die diesmal etwas anders aussieht. Zwar müssen wir auch hier wieder unseren SPO-Kontext auswählen, aber anders als zuvor mit den Standardgruppen haben wir hier die explizite Auswahl, welche von den Gruppen wir nehmen. 
  
  Durch das Setzen eines Hakens in den blauen Boxen wird die Wahl aktiv, unsere virtuelle Identität wählt entsprechend die Gruppen 11, 14, 2 und 3 und setzt die Prioritäten daneben entsprechend der Gewichtung, d.h. eine sehr hohe Präferenz für Gruppe 11, Mittel-Hohe für 14 und 2 und Mittlere für 3. Da unsere Identität sich der Risiken einer Nicht-Anmeldung mangels Platz bewusst ist, setzt es auch noch bei allen anderen Terminen, die nicht um 8:30 Uhr stattfinden (ja, Studenten sind faul!), den Haken und wählt eine niedrige Priorität. die 8:30-Uhr-Termine lässt es frei. Abbildung \ref{fig:afi-tut-prio} zeigt ein paar dieser Wahlen. 

  \begin{figure}[]
    \includegraphics[width=\textwidth]{./img/rwthonline-9.png}
    \caption{Auswahl der Tutorien entsprechend der Präferenzen für Analysis für Informatiker}
    \label{fig:afi-tut-prio}
  \end{figure}

  In AfI müssen wir noch eine weitere Sache beachten, die euch an mehreren Stellen in Modulen begegnen wird. Die abzugebenden Hausaufgaben sind meist in Gruppen- oder Partnerarbeit zu bearbeiten, um den Lehrstühlen Arbeit bei der Korrektur jede Woche zu sparen, ebenso um Teamarbeit zu fördern. Wenn ihr also schon wisst, mit wem ihr zusammen eure Hausaufgaben abgeben wollt, so könnt ihr eine Gruppe formen, indem ihr in dem Feld oben rechts, in dem \emph{Teamname} steht, den gleichen Gruppennamen eintragt. Idealerweise sind diese Gruppennamen eindeutig, sonst kommt es gerne mal zu Problemen bei der Zuordnung von Übungspunkten, seid also kreativ in der Namensgebung!

  Sobald auch das getan ist, können wir wieder mit \emph{Belegwunsch erfassen} unsere Wahl bestätigen und erhalten wieder einen Überblick über unsere Wahl. Wir erhalten wieder einige E-Mails und sobald unsere Wahl bestätigt wurde, werden die Tutorien auch im Terminkalender erscheinen. 

  Damit haben wir uns erfolgreich für alle vorerst verfügbaren LVs von AfI angemeldet.  

  \subsection*{Weitere Module}
  Die Prüfungsanmeldung selbst kann nach dem neuen System erst Mitte/Ende November bis in den Dezember hinein geschehen, wird sich aber ähnlich bedienen lassen und bis dahin habt ihr hoffentlich den Dreh raus.

  So oder ähnlich sehen auch die Anmeldeverfahren für die anderen Module aus, allerdings mit einigen Eigenheiten, die im Folgenden noch einmal betrachtet werden sollen:

  \begin{itemize}
    \item TI hat keine Tutorien, es gibt also nur eine Globalübung und eine Vorlesung, für die man sich anmelden kann. Insbesondere ist hinter dem Knoten \emph{Globalübung} bei TI nichts hinterlegt, der entsprechende Button ist unter dem grünen Knoten \emph{Übung Einführung in die Technische Informatik zu finden}
    \item Thematisch ist die Programmierungs-Vorlesung zweigeteilt und diese Teilung scheint sich auch in RWTHonline fortgesetzt zu haben. Die Anmeldung für Vorlesung und Übung erscheint daher in Teil 1 und Teil 2, allerdings soll das Anmelden, sobald dies freigeschaltet wird, automatisch beide Teile umfassen. 
    Sich die Termine für Teil 1 vorzumerken, fügt momentan auch die Termine von Teil 2 zum Vormerk-Kalender hinzu und vice versa, somit ist anzunehmen, dass die Anmeldung ähnlich verfährt.
    \item Die Anmeldung für Progra-Tutorien verläuft traditionell über die Website des Lehrstuhls selbst.\footnote{ https://verify.rwth-aachen.de/programmierungWS18/} Sobald dort eine Zuteilung erfolgt ist, können die Termine aus RWTHonline, wo diese ebenfalls gelistet, aber nicht zur Anmeldung freigegeben sind, in den Kalender eingetragen werden (Vermutlich mit der Vormerk-Option)
    \item In DS gibt es zwar Tutorien, diese scheinen aber momentan noch nicht in RWTHonline eingetragen zu sein. Wie die Zuteilung zu diesen stattfinden wird, wird Prof. Hiß wohl in den ersten Vorlesungen erklären. 
  \end{itemize}

  \section*{Persönlicher Terminkalender}
  RWTHonline bringt ein Tool zum Planen und Anzeigen des Stundenplans mit. Dieses ist zu Finden über die Kachel \emph{Persönlicher Terminkalender} aus der Startseite (s. Abbildung \ref{fig:startseite}).

  \begin{figure}[]
    \includegraphics[width=\textwidth]{./img/rwthonline-10.png}
    \caption{Exemplarischer Stundenplan}
    \label{fig:timetable}
  \end{figure}

  Standardmäßig zeigt dieser die gesamte aktuelle Woche an, mit dem Dropdown-Menü oben links kann man ihn in den Stundenplan-Modus bringen, sodass er die Serientermine anzeigt. Wenn ihr die Vormerk-Funktion benutzt, sind diese Termine standardmäßig \emph{nicht sichtbar} und wir müssen in den Einstellungen des Kalenders erst auswählen, dass dieser die vorgemerkten LVs anzeigt (s. Abbildung \ref{fig:timetable-options}).

  \begin{figure}[]
    \includegraphics[width=\textwidth]{./img/rwthonline-11.png}
    \caption{Einstellungen des Terminkalenders für vorgemerkte LVs}
    \label{fig:timetable-options}
  \end{figure}

  Je nach dem, wie sehr ihr mit dem Stern gegeizt habt, wird euer Terminkalender nun gepflastert sein mit Veranstaltungen und es ist dann eure administrative Aufgabe, dort für Ordnung zu sorgen, dafür wurde leider noch kein Tool entwickelt.

  Mit einem Klick auf \emph{Terminkollision} prüft RWTHonline, welche Termine sich überschneiden und wo entsprechend geschoben werden muss, wenn ihr alle Veranstaltungen wahrnehmen wollt. Dieses Tool erleichtert die Überschneidungsfindung, da der Stundenplan-Modus nicht jeden individuellen Termin betrachtet, sondern nur die Serientermine, die Kollisionsansicht hingegen schon, sollte es also zu einzelnen Überschneidungen kommen, findet ihr diese hier.

  Um den Terminkalender in externe Applikationen einzubinden, zum Beispiel den Google Calendar auf Android-Geräten etc., kann man nutzen, dass sich der Kalender entweder fix als Ganzes exportieren und in der anderen Applikation wieder importieren lässt, oder sogar einbetten lässt, sodass bei der Terminänderung zwischen den beiden Kalendern synchronisiert wird.

  Mit einem Klick auf \emph{Veröffentlichen} kann eine solche URL generiert werden, die zur Einbettung genutzt werden kann, mit \emph{Exportieren} entsprechend der statische Export vollzogen werden.

  \section*{Abschließende Worte}
  Ich möchte zum Schluss nochmal betonen, dass Mentoring für Informatiker eine Pflichtveranstaltung ist und ihr euch dafür ebenfalls anmelden müsst. Allerdings ist diese Anmeldung momentan noch nicht möglich, was ihr allerdings machen könnt, um die gewünschten Termine in den Kalender zu bekommen, ist euch Mentoring Gruppe 27 vorzumerken, dass ist die ehemalige Gruppe \LaTeX, nach der Erstiwoche werden wir nur noch auf eine Zahl reduziert. 

  Ich hoffe weiterhin, dass dieser Guide euch geholfen hat, wenn sich jetzt irgendwelche Fragen aufgetan haben oder sich in Zukunft auftun, scheut euch nicht, zu fragen, primär am besten per E-Mail an \begin{center}
    \texttt{alexander.bartolomey@rwth-aachen.de}
  \end{center}
  sonst aber auch über beliebige andere Kanäle, auf denen ihr sicher seid, mich zu erreichen. 

  Viel Erfolg bei dem Versuch, RWTHonline zu verstehen, wir sind die Ersten und damit ist das für uns alle ein Sprung ins kalte Wasser!
\end{document}